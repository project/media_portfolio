<?php
// $Id:
/**
 * @file
 * media_portfolio_hooks.inc
 *
 * Created by: william roboly
 * Date: 2008-03-31
 * (c) Copyright 2008 Openject Consulting. All Rights Reserved. 
 */

/****************************************************************************
 *
 * Hooks Functions
 *
 ****************************************************************************/

/**
 * Implementation of hook_help()
 */
function media_portfolio_help($section) {
  switch ($section) {
    case 'admin/settings/media_portfolio':
      $output = t('Configure Media Portfolio settings');
      break;    
    case 'admin/help#description':
      return t("This module provides a simple and effective way for users to manage uploaded assets (image, video, audio) and for site administrators to visual represent these assets on the User Profiles page in a clean and effective way.");
  }
}

/**
 * Implementation of hook_perm
 */
function media_portfolio_perm() {
  return array(
    MEDIA_PORTFOLIO_PERM_VIEW, 
    MEDIA_PORTFOLIO_PERM_EDIT, 
    MEDIA_PORTFOLIO_PERM_ADMIN
  );
}

/**
 * Implementation of hook_menu()
 */
function media_portfolio_menu($may_cache) {
  $items = array();
  global $user;

  if ($may_cache) {
    $items[] = array(
      'path'     => 'admin/settings/media_portfolio',
      'title'    => t('Media Portfolio'),
      'description' => t('Configure Media Portfolio settings.'),
      'callback' => 'drupal_get_form',
      'callback arguments' => array('media_portfolio_content_settings'),
      'access'   => user_access(USER_SITE_PERM_ADMIN),
      'type'     => MENU_NORMAL_ITEM,
    );

    $items[] = array(
			'path' => 'admin/settings/media_portfolio/content',
      'title' => t('Content settings'),
      'type' => MENU_DEFAULT_LOCAL_TASK,
      'weight' => -10
    );

	  $items[] = array(
	    'path' => 'admin/settings/media_portfolio/display',
	    'title' => t('Display settings'),
	    'callback' => 'drupal_get_form',
	    'callback arguments' => array('media_portfolio_display_settings'),
	    'access' => user_access(USER_SITE_PERM_ADMIN),
	    'type' => MENU_LOCAL_TASK,
	    'weight' => 0,
	  );

    $items[] = array(
      'path' => 'admin/settings/media_portfolio/advanced',
      'title' => t('Advanced settings'),
      'callback' => 'drupal_get_form',
      'callback arguments' => array('media_portfolio_advanced_settings'),
      'access' => user_access(USER_SITE_PERM_ADMIN),
      'type' => MENU_LOCAL_TASK,
      'weight' => 10,
    );

    // Show page of ALL User Media Portfolios in condensed format
    $items[] = array(
      'path'     => 'media_portfolios',
      'title'    => t('Media Portfolios'),
      'callback' => 'media_portfolio_list_portfolios',
      'access'   => user_access(MEDIA_PORTFOLIO_PERM_VIEW),
      'type' => MENU_NORMAL_ITEM,
    );

    // Show User's Media Portfolio management page
    $items[] = array(
      'path'     => 'my_media_portfolio',
      'title'    => t('My Media Portfolio'),
      'callback' => 'media_portfolio_my_media_portfolio',
      'access'   => (user_access(MEDIA_PORTFOLIO_PERM_EDIT) && $user->uid),
      'type' => MENU_NORMAL_ITEM,
    );
	}

  return $items;
}

/**
 * Implementation of hook_user
 */
function media_portfolio_user($op, &$edit, &$account, $category = NULL) {
  switch ($op) {
    case 'view':
	    global $user;
	    $viewer =& $user;

      $category = t('Media Portfolio');
	
			// Check to see if we are to use Views or our default mechanism
			if (variable_get('media_portfolio_toggle_views', 0) == 0) {
				$value = theme('media_portfolio_display_content', $account->uid);
			}
			else {
				$value = theme('media_portfolio_views_display_content', $account->uid);
			}

	    if(sizeof($value)) {
	      return array($category => $value);
	    }
      break;
  }
}
/**
 * Implementation of hook_nodeapi()
 */
function media_portfolio_nodeapi(&$node, $op, $a3 = NULL, $a4 = NULL) {
  global $user;
  
  switch ($op) {
    case 'insert':
    
      // If a profile node is being created, add it to the user profile
      // queue so that it can be selected for a "profile of the day" view.
      //if (($node->type == 'profile') and (module_exists('nodequeue'))) {
      //  nodequeue_queue_add(MJF_QUEUE_USER_PROFILE, $node->nid);
      //}
    
      break;
		case 'update':
			break;
    case 'prepare':
      break;
    case 'validate':
      break;
		case 'view':
			break;
		case 'delete':
			break;
  }
}
?>
