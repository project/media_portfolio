<?php
// $Id:
/**
 * @file
 * media_portfolio_theme.inc
 *
 * Created by: william roboly
 * Date: 2008-03-31
 * (c) Copyright 2008 Openject Consulting. All Rights Reserved. 
 */

/****************************************************************************
 *
 * Theme Functions
 *
 ****************************************************************************/

/**
 * undocumented function
 *
 * @param $uid
 *    integer value of the User ID
 *
 * @return $output
 **/
function theme_media_portfolio_display_content($uid) {
  $result = array();
  
  $data_rows    = media_portfolio_user_profile_rows($uid);
  $data_by_type = media_portfolio_group_data_by_type($data_rows);
  
  if (count($data_by_type)) {
    foreach($data_by_type as $type => $rows) {
      if (count($rows)) {
        $value .= theme(
          'table', 
          array(t('Title'), t('Created')), 
          $rows
        );
      } else {
				$value = "None";
			}
    }
	  $output[] = array(
	    'title' => $type,
	    'value' => $value, 
	    'class' => 'media_portfolio ' . $type,
	  );
  }

  return $output;  
}


/**
 * undocumented function
 *
 * @param $uid
 *    integer value of the User ID
 *
 * @return $output
 **/
function theme_media_portfolio_views_display_content($uid) {
  $output = array();

  $view_assets = media_portfolio_get_views();

  foreach ($view_assets as $type => $array) {
    $obj = (object)$array;

    if ($obj->view != 'none') {
      $view = views_get_view($obj->view);
      $value = views_build_view('embed', $view, array(0 => $uid, 1 => $obj->nodetype));
    }

	  $output[] = array(
	    'title' => $obj->title,
	    'value' => $value, 
	    'class' => 'media_portfolio ' . $obj->nodetype,
	  );

  }
  return $output;
}

?>