<?php
// $Id:
/**
 * @file
 * media_portfolio_forms.inc
 *
 * Created by: william roboly
 * Date: 2008-03-31
 * (c) Copyright 2008 Openject Consulting. All Rights Reserved. 
 */

/****************************************************************************
 *
 * Form Functions
 *
 ****************************************************************************/

function media_portfolio_settings_submit($form_id, $form_values) {
  if ($form_values['op'] == t('Enable')) {
    variable_set('media_portfolio_content_types_enabled', 1);
    // TODO: function to build CCK node types here
    _mp_create_content_types();
    drupal_set_message(t('The Media Portfolio content types have been created.'));
  }
  elseif ($form_values['op'] == t('Disable')) {
    variable_set('media_portfolio_content_types_enabled', 0);
    // TODO: function to remove CCK node types here
    _mp_delete_content_types();
    drupal_set_message(t('The Media Portfolio content types have been removed.'));
  }
}

/**
 * menu callback for settings form.
 */
function media_portfolio_content_settings() {
  $form = array();

  // Initial form group
  $group = 'media_portfolio_settings';
  // custom submit handler
  $form['#submit'][$group . '_submit'] = array(); 
  // form.inc never calls the $callback if a submit handler is defined
  $form['#submit']['system_settings_form_submit'] = array(); 
  drupal_set_title(t('Media Portfolio Content Settings'));
  
  $form['description'] = array(
    '#type' => 'item',
    '#title' => '',
    '#value' => t('The Media Portfolio module provides an out-of-the-box set of controls for the presentation and management of media assets. The module also exposes users and administrators to default settings for easy integration with various other modules (e.g.: !modules). The goal of this module is to simplify the implementation and maintenance of a media rich website.', array('!modules' => _mp_modules_listing())),
//module content types for each asset type. After saving this, configuration options will appear for all of the node types that you specified. After selecting appropriate settings for each node type, please save again. The !advanced and !display pages use the settings on this page, so be sure to save these settings before proceeding to either of them.', array('!display'=>l('Display settings', 'admin/settings/media_portfolio/display'), '!advanced'=>l('Advanced settings', 'admin/settings/media_portfolio/advanced'))),
    '#weight' => -10,
  );

  if (variable_get('media_portfolio_content_types_enabled', 0)) {
    $collapsed = TRUE;  
    $status = t('enabled');
    $btn_text = t('Disable');
    $description = t('With Media Portfolio content types enabled, as long as all the proper modules are also enabled, you should find the assets its creates sufficient for all your needs. If you need to add fields or changed them in anyway, use the !content page to edit them. If you wish to use your own, disable these ones or do not choose them in the options below.');
  }
  else {
    $collapsed = FALSE;
    $status = t('disabled');
    $btn_text = t('Enable');
    $description = t('Enable Media Portfolio content types. Please note: should any custom content type resemble the naming convention of this modules exposed types, there could be some data loss. Please back up your DB before enabling this function. Should you disable these later, the nodes this module created will be <strong>destroyed</strong> unless kept in a safe place, like a hamper or DB dump.');
  }

  $form[$group]['media_portfolio_status'] = array(
    '#type' => 'fieldset', 
    '#title' => t('Media Portfolio content type control'), 
    '#description' => $description, 
    '#collapsible' => TRUE, 
    '#collapsed' => $collapsed,
  );
  $form[$group]['media_portfolio_status']['content_types_action'] = array(
    '#type' => 'submit', 
    '#value' => $btn_text, 
    '#prefix' => '<p>'. t('Media Portfolio content types are %status.', array('%status' => $status)). '</p>');

/*
  TODO: Add the Album collection feature to the module

  $form[$group]['media_portfolio_type_album'] = array(
    '#type'           => 'select',
    '#title'          => t('Album Content Type'),
    '#default_value'  => variable_get('media_portfolio_type_album', 'default'),
    '#options'        => _mp_contenttype_list(),
    '#description'    => t('Select from this list of content types which one Media Portfolio should use to represent an Album asset? (a collection of nodes to more easily manage large data sets.)'),
  );
*/

  $form[$group][MEDIA_PORTFOLIO_TYPE_AUDIO] = array(
    '#type'           => 'select',
    '#title'          => t('Audio Content Type'),
    '#default_value'  => variable_get(MEDIA_PORTFOLIO_TYPE_AUDIO, 'default'),
    '#options'        => _mp_contenttype_list(),
    '#description'    => t('Select from this list all the content types Media Portfolio should use to represent Audio assets?'),
    //'#size'           => count(_mp_contenttype_list()),
    //'#multiple'       => TRUE,
  );

  $form[$group][MEDIA_PORTFOLIO_TYPE_IMAGE] = array(
    '#type'           => 'select',
    '#title'          => t('Image Content Type'),
    '#default_value'  => variable_get(MEDIA_PORTFOLIO_TYPE_IMAGE, 'default'),
    '#options'        => _mp_contenttype_list(),
    '#description'    => t('Select from this list all the content types Media Portfolio should use to represent Image assets?'),
    //'#size'           => count(_mp_contenttype_list()),
    //'#multiple'       => TRUE,
  );

  $form[$group][MEDIA_PORTFOLIO_TYPE_VIDEO] = array(
    '#type'           => 'select',
    '#title'          => t('Video Content Type'),
    '#default_value'  => variable_get(MEDIA_PORTFOLIO_TYPE_VIDEO, 'default'),
    '#options'        => _mp_contenttype_list(),
    '#description'    => t('Select from this list all the content types Media Portfolio should use to represent Video assets?'),
    //'#size'           => count(_mp_contenttype_list()),
    //'#multiple'       => TRUE,
  );

//  $form['setting'] = module_invoke_all('media_portfolio', 'setting');
  return system_settings_form($form);
}

/**
 * menu callback for display settings form.
 */
function media_portfolio_display_settings() {
  $form = array();

  drupal_set_title(t('Media Portfolio Display Settings'));

	$mp_toggle_views_options_value = variable_get('media_portfolio_toggle_views', 0);
	$mp_toggle_views_options = array(t('Default'), t('Views'));

  $form['description'] = array(
    '#type' 					=> 'item',
    '#title' 					=> t('General Display Settings'),
    '#value' 					=> '<p>' . t('The display settings for Media Portfolio content types provide several ways of controlling the presentation of media assets through blocks, views, pages and user profile categories.') . '</p>',
    '#weight' 				=> -10,
  );

	$form['media_portfolio_toggle_views'] = array(
	  '#type' 					=> 'radios',
	  '#title'          => t('Display mechanism'),
	  '#options'        => $mp_toggle_views_options,
	  '#description'    => t('Select which mechanism to use as the default presentation system.'),
	  '#default_value'  => $mp_toggle_views_options_value,
    '#weight' 				=> -9,
	);

  return system_settings_form($form);
}

/**
 * menu callback for advanced settings form.
 */
function media_portfolio_advanced_settings() {
  $form = array();

  // Initial form group
  $group = 'media_portfolio_advanced_settings';
  drupal_set_title(t('Media Portfolio Advanced Settings'));
  
  $form['description'] = array(
    '#type' => 'item',
    '#title' => '',
    '#value' => t('Below are the advanced settings for media portfolio. <p><strong>N.B.</strong>: Please be very careful and backup all your material before playing around with these settings.</p>'),
    '#weight' => -10,
  );
  
  return system_settings_form($form);
}

?>