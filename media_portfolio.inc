<?php
// $Id:
/**
 * @file
 * media_portfolio.inc
 *
 * Created by: william roboly
 * Date: 2008-03-31
 * (c) Copyright 2008 Openject Consulting. All Rights Reserved. 
 */

/****************************************************************************
 *
 * Functions
 *
 ****************************************************************************/

/**
 * Generates an array of named nodes keyed by node type.
 *
 * @param $use_blank string value
 *    Use this string to represent the "blank" value in your list
 * @param $show_all boolean value
 *    TRUE means show complete list of content types
 *    FALSE means show only those content types specified by the value
 *      of $type_var
 * @param $type_var string value
 *    Variable name pointing to an array of related content types 
 *
 * @return $node_list array
 *    An array of content types available
 */
function _mp_contenttype_list($use_blank = '', $show_all = TRUE, $type_var = '') {
  $node_list = array();

  if ($show_all || $type_var == '') {
    $node_list = node_get_types('names');
  } 
  else {
    $show_nodes = variable_get($type_var, array());
    foreach($show_nodes as $key=>$type) {
      if ($type != 'default') { 
        $node_list[$type] = node_get_types('name',$type);
      }
    }
  }

  if ($use_blank) {
    $node_list['default'] = t($use_blank);
  }

  return $node_list;
}

/**
 * Create Content Types
 *  This function will create the required content types for Media Portfolio
 *  defaults. The Administrator is not required to use these. In fact, should 
 *  they already have the content types created, on the content_settings page 
 *  are a set of select boxes for them to assign media asset designations.
 *
 * @return boolean
 *
 */
function _mp_create_content_types() {
  include_once('./'. drupal_get_path('module', 'node') .'/content_types.inc');
  include_once('./'. drupal_get_path('module', 'content') .'/content_admin.inc');

  $type_array = media_portfolio_media_types();
  // TODO: Add check to see if the proper modules_exist for these CCK import scripts 
  //        to work properly. Otherwise, we need to tell the Admin about the 
  //        module discrepency. Maybe, it would be wise to forse module dependencies,
  //        but then that just a means we lose on flexibility.
  foreach($type_array as $key => $values) {
    _mp_create_content_type(MEDIA_PORTFOLIO_PATH . '/' . $values['file']);
    if (module_exists('og')) {
      _mp_og_omitted_set($values['type']);
    }
  }
    
  return $value;
}

/**
 * Set Organic Groups Omitted content type function
 *
 * @param $type
 *    string value of the content type to be added to the og_omitted variable
 *
 * @return void
 *
 **/
function _mp_og_omitted_set($type) {
  $og_omitted = variable_get('og_omitted', array());
  $og_omitted_array = array($type => $type);
  $og_omitted_final = array_merge($og_omitted, $og_omitted_array);
  variable_set('og_omitted', $og_omitted_final);
}

/**
 * Delete Content Types
 *
 * @return void
 *
 */
function _mp_delete_content_types() {
  $type_array = media_portfolio_media_types();
  foreach($type_array as $key => $values) {
    node_type_delete($values['type']);
  }
}

/**
  * Helper function to import a CCK content type definition from a text file.
  *
  * @param $cck_definition_file
  *   The full path to the file containing the CCK definition.
  *
  * @return void
  *
  */
function _mp_create_content_type($cck_definition_file) {
  if (file_exists($cck_definition_file)) {
    $values = array();
    $values['type_name'] = '<create>';
    $values['macro'] = file_get_contents($cck_definition_file);
    drupal_execute("content_copy_import_form", $values);
  }
}

/**
 * Helper function to output a list of candidate modules which the 
 * Media Portfolio can interact with.
 *
 * @return string
 *
 **/
function _mp_modules_listing() {
  
	// Create an array of key/values representing the module's name and the
	// project title
	$listing = array(
		'nodequeue' 		=> 'NodeQueue',
		'imagecache' 		=> 'ImageCache',
		'flashvideo' 		=> 'FlashVideo',
		'mediafield' 		=> 'MediaField',
		'imagefield' 		=> 'ImageField',
	);

	foreach ($listing	as $key => $value) {
		$output[] = l($value, MEDIA_PORTFOLIO_DO_PROJECT_PATH . $key, array('target' => '_blank', 'style' => 'font-weight: bold;'));
	}

	$output = implode(', ', $output);

  return t($output);
}


/****************************************************************************
 *
 * Views Helper Functions
 *
 ****************************************************************************/
 
/**
 * Helper function to return an array of all Views available for use with
 * the Media Portfolio. Each entry must begin with the prefix mp_ otherwise
 * it could return a massive list of options which we do not need.
 *
 * What can I say, I'm a control freak.
 *
 * @return $views 
 *    array() value
 * 
 */
function _mp_get_available_views() {
  $render_opts = array(
    'none' => t('no views - use default presentation')
  );  
  
  $views = array();
  $result = db_query("SELECT name, description FROM {view_view} WHERE name LIKE 'mp_%' ORDER BY name");
  while ($view = db_fetch_object($result)) {
    $views[t('Existing Views')][$view->name] =  check_plain("$view->name - $view->description");
  }
  
  views_load_cache(); // the function below was not loaded without this call
  $default_views = _views_get_default_views();
  $views_status = variable_get('views_defaults', array());

  foreach ($default_views as $view) {
    // filter this list to simplify for admins. 
    if (substr($view->name, 0, 3) == 'mp_') {
      if (!$views[$view->name] && 
        ($views_status[$view->name] == 'enabled' || (!$views_status[$view->name] && !$view->disabled))) {
        $views[t('Default Views')][$view->name] = "$view->name - $view->description";
      }
    }
  } 
  
  $views = array_merge($render_opts, $views);

  return $views;
}
?>